#include "FastCGIResponse.hpp"

#include <algorithm>

FastCGIResponse::FastCGIResponse(const String &content_type)
{
    // Mandatory! Otherwise FastCGI reports a 502 Bad Gateway
    this->addHttpHeader("Content-Type", content_type);

    // Set HTTP status
    this->setStatus(200); // OK
}

FastCGIResponse::~FastCGIResponse()
{
    this->headers.clear();
    this->body.clear();
}

void FastCGIResponse::setStatus(unsigned short status)
{
    if (this->hasHttpHeader("Status"))
        this->removeHttpHeader("Status");
    this->addHttpHeader("Status", std::to_string(status));
}

void FastCGIResponse::addHttpHeader(const HttpHeader &header)
{
    this->addHttpHeader(header.first, header.second);
}

void FastCGIResponse::addHttpHeader(const String &name, const String &value)
{
    if (!this->hasHttpHeader(name))
        this->headers.push_back(HttpHeader(name, value));
}

void FastCGIResponse::removeHttpHeader(const HttpHeader &header)
{
    this->removeHttpHeader(header.first);
}

void FastCGIResponse::removeHttpHeader(const String &name)
{
//    this->headers.erase(std::remove_if(
//        this->headers.begin(),
//        this->headers.end(),
//        [name](const String &item) {
//            return item == name;
//        }), this->headers.end());

    for (std::size_t i = 0; i < this->headers.size(); i++)
    {
        if (this->headers.at(i).first == name)
        {
            this->headers.erase(this->headers.begin() + i);
            return;
        }
    }
}

bool FastCGIResponse::hasHttpHeader(const HttpHeader &header) const
{
    return this->hasHttpHeader(header.first);
}

bool FastCGIResponse::hasHttpHeader(const String &name) const
{
    for (auto&& h : this->headers)
        if (h.first == name)
            return true;
    return false;
}

void FastCGIResponse::setBody(const String &body)
{
    this->body = body;
}

void FastCGIResponse::clearBody()
{
    this->body.clear();
}

void FastCGIResponse::append(const String &str)
{
    this->body.append(str);
}
void FastCGIResponse::append(const char *str)
{
    this->body.append(str);
}

void FastCGIResponse::appendNewline()
{
    this->body.append("\n");
}

const String FastCGIResponse::getHeaders() const
{
    String headers;
    for (auto&& h : this->headers)
        headers.append(h.first + ": " + h.second + "\n");
    return headers;
}

const String FastCGIResponse::getResponse() const
{
    String response;

    // append headers
    for (auto&& h : this->headers)
        response.append(h.first + ": " + h.second + "\r\n");
    response.append("\r\n");

    // append body
    response.append(this->body);

    return response;
}

void FastCGIResponse::sendResponse() const
{
    std::printf("%s", this->getResponse().c_str());
}
