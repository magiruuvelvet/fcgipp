#include "Vector.hpp"
#include "String.hpp"

bool strvector_str_contains(const Vector<String> &strvec, const String &str, int *pos)
{
    if (pos)
    {
        for (std::size_t i = 0; i < strvec.size(); i++)
        {
            if (strvec.at(i).contains(str))
            {
                *pos = i;
                return true;
            }
        }
        *pos = -1;
        return false;
    }
    else
    {
        for (auto&& i : strvec)
            if (i.contains(str))
                return true;
        return false;
    }
}

bool strvector_str_starts_with(const Vector<String> &strvec, const String &str, int *pos)
{
    if (pos)
    {
        for (std::size_t i = 0; i < strvec.size(); i++)
        {
            if (strvec.at(i).starts_with(str))
            {
                *pos = i;
                return true;
            }
        }
        *pos = -1;
        return false;
    }
    else
    {
        for (auto&& i : strvec)
            if (i.starts_with(str))
                return true;
        return false;
    }
}
