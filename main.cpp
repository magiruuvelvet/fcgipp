#include "FastCGI.hpp"
#include "FastCGIResponse.hpp"
#include "UserAgent.hpp"

int main(void)
{
    if (!FastCGI::IsRunningUnderFastCGI())
    {
        std::printf("Application must be invoked by a FastCGI server!\n");
        return 5;
    }

    // Send HTTP headers
    // ---TODO: improve the usage of this class
    FastCGIResponse res;

    // Only respond with data on GET requests
    if (FastCGI::GetHttpRequestMethod() != FastCGI::GET)
    {
        res.append("Only GET requests are allowed!\n");
        res.setStatus(400); // Bad request
        res.sendResponse();
        return 1;
    }

    auto path = FastCGI::QueryStringValue("path");
    if (path == "/")
    {
        res.append("Secure Connection: " + String(FastCGI::IsServedViaTLS() ? "yes" : "no") + "\n");
        res.append("Client requested secure connection: " + String(FastCGI::UpgradeInsecureRequests() ? "yes" : "no") + "\n");
        res.append("User Agent:        " + FastCGI::UserAgentString() + "\n");
        res.append("Do Not Track:      " + String(FastCGI::DoNotTrackEnabled() ? "yes" : "no") + "\n");
        res.append("Application Path:  " + FastCGI::ApplicationPath() + "\n");
        res.appendNewline();

        FastCGI::PrintCGIServerInfo(res);
        FastCGI::PrintHTTPHeaders(res);
        //FastCGI::PrintProcessEnvironment(res);

        res.append(res.getHeaders());
    }
    else if (path == "/ua")
    {
        res.append("User Agent\n\n");
        res.append("Full User Agent:       " + FastCGI::UserAgentString() + "\n");
        res.append("\n\n");
        res.append("Platform:              " + FastCGI::UserAgent().Platform + "\n");
        res.append("Platform Version:      " + FastCGI::UserAgent().PlatformVersion + "\n");
        res.append("Browser:               " + FastCGI::UserAgent().Browser + "\n");
        res.append("Browser Version:       " + FastCGI::UserAgent().BrowserVersion + "\n");
        res.append("Graphical Environment: " + FastCGI::UserAgent().GraphicalEnvironment + "\n");
        res.append("Operating System:      " + FastCGI::UserAgent().OperatingSystem + "\n");
        res.append("OS Architecture:       " + FastCGI::UserAgent().OSArch + "\n");
        res.appendNewline();
        res.append("has WebEngine:    " + String(FastCGI::UserAgent().hasWebEngine() ? "yes" : "no") + "\n");
        res.append("Engines:          " + ([]{
            String s;
            for (auto&& e : FastCGI::UserAgent().Engines)
                s.append(e.first + '/' + e.second + ", ");
            s = s.substr(0, s.size() - 2);
            return s;
        })() + "\n");
        res.appendNewline();
        res.append("is dl-manager:    " + String(FastCGI::UserAgent().isDownloadManager() ? "yes" : "no") + "\n");
        res.append("is curl:          " + String(FastCGI::UserAgent().isCurl() ? "yes" : "no") + "\n");
        res.append("is wget:          " + String(FastCGI::UserAgent().isWget() ? "yes" : "no") + "\n");
        res.appendNewline();
        res.append("DEBUG:            " + FastCGI::UserAgent().DEBUG + "\n");
        res.appendNewline();
    }
    else if (path == "/qs")
    {
        res.append("test1: " + FastCGI::QueryStringValue("test1") + "\n");
        res.append("test2: " + FastCGI::QueryStringValue("test2") + "\n");

        res.appendNewline();

        res.append("has test3:       " + String(FastCGI::QueryStringHasKey("test3") ? "yes" : "no") + "\n");
        res.append("test4 has value: " + String(FastCGI::QueryStringKeyHasValue("test4") ? "yes" : "no") + "\n");
    }
    else
    {
        res.append(path + " not found!\n");
        res.setStatus(404);
    }

    res.sendResponse();

    return 0;
}
