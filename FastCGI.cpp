#include "FastCGI.hpp"
#include "FastCGIResponse.hpp"
#include "UserAgent.hpp"

#include <cstdlib>
#include <cstdio>
#include <algorithm>

// Checks if the application was invoked by a FastCGI server or standalone
bool FastCGI::IsRunningUnderFastCGI()
{
    return !(
        GetEnvironmentVariablePrivate("GATEWAY_INTERFACE").empty() ||                 // CGI Gateway Interface Type
        GetEnvironmentVariablePrivate("GATEWAY_INTERFACE").not_starts_with("CGI") ||  // Check if we are on a FastCGI server
        GetEnvironmentVariablePrivate("REMOTE_ADDR").empty() ||                       // Remote CGI Server Address
        GetEnvironmentVariablePrivate("SERVER_PROTOCOL").empty() ||                   // Server Protocol
        GetEnvironmentVariablePrivate("SERVER_NAME").empty()                          // Server Name (usually Domain Name)
    );
}

// Checks if the application is served via TLS (HTTPS)
bool FastCGI::IsServedViaTLS()
{
    return GetEnvironmentVariablePrivate("HTTPS") == "on";
}

bool FastCGI::UpgradeInsecureRequests()
{
    if (IsServedViaTLS())
        return false;
    const bool upgradeInsecure =
        GetEnvironmentVariablePrivate("HTTP_UPGRADE_INSECURE_REQUESTS") == "1"
        ? true : false;
    return upgradeInsecure;
}

bool FastCGI::DoNotTrackEnabled()
{
    return GetEnvironmentVariablePrivate("HTTP_DNT") == "1";
}

const String &FastCGI::UserAgentString()
{
    static const String ua = ([]{
        char *ua = getenv("HTTP_USER_AGENT");
        return ua ? String(ua) : String();
    })();
    return ua;
}

const UserAgent &FastCGI::UserAgent()
{
    static const ::UserAgent ua = ([]{
       return ::UserAgent(FastCGI::UserAgentString());
    })();
    return ua;
}

// For debugging purposes, pretty-prints the entire CGI-specific environment without HTTP headers
void FastCGI::PrintCGIServerInfo(FastCGIResponse &res)
{
    const auto print_fastcgi_var = [&](FASTCGI_VARIABLE cgivar, const char *dspname) {
        res.append(String(dspname) + ": " + GetFastCGIVariable(cgivar) + "\n");
    };

    res.append("FastCGI Server Info\n");
    print_fastcgi_var(FastCGI::GATEWAY_INTERFACE,  "Gateway Interface");
    print_fastcgi_var(FastCGI::REMOTE_ADDR,        "Remote Address");
    print_fastcgi_var(FastCGI::REMOTE_IDENT,       "Remote Identification");
    print_fastcgi_var(FastCGI::REMOTE_USER,        "Remote User");
    print_fastcgi_var(FastCGI::PATH_INFO,          "Path Info");
    print_fastcgi_var(FastCGI::PATH_TRANSLATED,    "Path Info (Translated)");
    res.appendNewline();

    res.append("FastCGI Host/Upstream Server Info\n");
    print_fastcgi_var(FastCGI::SERVER_PROTOCOL,    "Server Protocol");
    print_fastcgi_var(FastCGI::SERVER_SOFTWARE,    "Server Software");
    print_fastcgi_var(FastCGI::SERVER_NAME,        "Server Name / Domain");
    print_fastcgi_var(FastCGI::SERVER_PORT,        "Server Port");
    print_fastcgi_var(FastCGI::SCRIPT_NAME,        "CGI Script Name");
    print_fastcgi_var(FastCGI::DOCUMENT_ROOT,      "Document Root");
    res.appendNewline();

    res.append("FastCGI HTTP Properties\n");
    print_fastcgi_var(FastCGI::REQUEST_METHOD,     "HTTP Request Method");
    print_fastcgi_var(FastCGI::DOCUMENT_URI,       "HTTP Request Path"); // invisible rewritten URI on server
    print_fastcgi_var(FastCGI::REQUEST_URI,        "HTTP Request URI");  // requested URI
    print_fastcgi_var(FastCGI::QUERY_STRING,       "HTTP Query String");
    print_fastcgi_var(FastCGI::AUTH_TYPE,          "Authentification Type");
    res.appendNewline();

    res.append("FastCGI HTTP Content Info\n");
    print_fastcgi_var(FastCGI::CONTENT_TYPE,       "Content Type");
    print_fastcgi_var(FastCGI::CONTENT_LENGTH,     "Content Length");
    res.appendNewline();
}

// For debugging purposes, pretty-print all HTTP Headers (HTTP_*)
// Be aware this may contain plain text passwords when using the HTTP basicauth feature !!!
void FastCGI::PrintHTTPHeaders(FastCGIResponse &res, DEBUG_HTTP_HEADERS type)
{
    res.append("FastCGI HTTP Headers\n");

    extern char **environ;
    for (char **env = environ; *env != nullptr; env++)
    {
        const String thisEnv(*env);
        if (thisEnv.starts_with("HTTP"))
        {
            ///
            /// FIXME: simplify this
            ///
            auto splitpos = thisEnv.find_first_of('=');
            if (splitpos != String::npos)
            {
                // Print all or standard HTTP headers
                if (type == All || type == Standard)
                res.append(thisEnv.substr(0, splitpos) + ": " +
                               thisEnv.substr(splitpos + 1) + "\n");

                // Print custom (X-) HTTP headers
                else if (type == Custom && thisEnv.starts_with("HTTP_X"))
                res.append(thisEnv.substr(0, splitpos) + ": " +
                               thisEnv.substr(splitpos + 1) + "\n");
            }
            else
            {
                res.append(thisEnv + "\n");
            }
        }
    }
    res.appendNewline();
}

// For debugging purposes, print entire process environment (output may be ugly)
// Be aware this may contain personal information, don't use this in production !!!
void FastCGI::PrintProcessEnvironment(FastCGIResponse &res)
{
    res.append("FastCGI Process Environment\n");

    extern char **environ;
    for (char **env = environ; *env != nullptr; env++)
    {
        char *thisEnv = *env;
        res.append(String(thisEnv) + "\n");
    }
    res.appendNewline();
}

const String FastCGI::ApplicationPath()
{
    return FastCGI::GetFastCGIVariable(DOCUMENT_ROOT);
}

// Most common HTTP request methods
FastCGI::HTTP_REQUEST_METHOD FastCGI::GetHttpRequestMethod()
{
    // Get the request method from the RAW getter
    const String req = GetHttpRequestMethodRaw();

    // Determine the request method and convert it into an enumerator
         if (req == "HEAD")      return HEAD;
    else if (req == "OPTIONS")   return OPTIONS;
    else if (req == "GET")       return GET;
    else if (req == "POST")      return POST;
    else if (req == "PUT")       return PUT;
    else if (req == "PATCH")     return PATCH;
    else if (req == "DELETE")    return DELETE;
    else if (req == "TRACE")     return TRACE;
    else if (req == "CONNECT")   return CONNECT;
    else return INVALID;
}

// HTTP allows custom request methods
const String FastCGI::GetHttpRequestMethodRaw()
{
    // Get and unify the request method from FastCGI
    return GetEnvironmentVariablePrivate("REQUEST_METHOD").upper();
}

bool FastCGI::QueryStringHasKey(const String &key)
{
    const String query = FastCGI::GetFastCGIVariable(QUERY_STRING);
    auto qkv = query.split('&');
    return strvector_str_starts_with(qkv, key);
}

bool FastCGI::QueryStringKeyHasValue(const String &key)
{
    const String query = FastCGI::GetFastCGIVariable(QUERY_STRING);
    auto qkv = query.split('&');

    int pos;
    strvector_str_starts_with(qkv, key, &pos);

    if (pos == -1)
        return false;

    return qkv.at(pos).starts_with(key + "=");
}

const String FastCGI::QueryStringValue(const String &key)
{
    const String query = FastCGI::GetFastCGIVariable(QUERY_STRING);
    auto key_pos = query.find(key + "=");
    if (key_pos == String::npos)
        return String();
    auto start_pos = key_pos + key.size() + 1;
    auto end_pos = query.find('&', start_pos);
    return query.substr(start_pos, end_pos - start_pos);
}

const String FastCGI::GetFastCGIVariable(FASTCGI_VARIABLE cgivar)
{
    switch (cgivar)
    {
        case GATEWAY_INTERFACE: return GetEnvironmentVariablePrivate("GATEWAY_INTERFACE"); break;
        case REMOTE_ADDR:       return GetEnvironmentVariablePrivate("REMOTE_ADDR"); break;
        case REMOTE_IDENT:      return GetEnvironmentVariablePrivate("REMOTE_IDENT"); break;
        case REMOTE_USER:       return GetEnvironmentVariablePrivate("REMOTE_USER"); break;
        case PATH_INFO:         return GetEnvironmentVariablePrivate("PATH_INFO"); break;
        case PATH_TRANSLATED:   return GetEnvironmentVariablePrivate("PATH_TRANSLATED"); break;

        case SERVER_PROTOCOL:   return GetEnvironmentVariablePrivate("SERVER_PROTOCOL"); break;
        case SERVER_SOFTWARE:   return GetEnvironmentVariablePrivate("SERVER_SOFTWARE"); break;
        case SERVER_NAME: case DOMAIN_NAME:
            return GetEnvironmentVariablePrivate("SERVER_NAME"); break;
        case SERVER_PORT:       return GetEnvironmentVariablePrivate("SERVER_PORT"); break;
        case SCRIPT_NAME:       return GetEnvironmentVariablePrivate("SCRIPT_NAME"); break;
        case DOCUMENT_ROOT:     return GetEnvironmentVariablePrivate("DOCUMENT_ROOT"); break;

        case REQUEST_METHOD:    return GetEnvironmentVariablePrivate("REQUEST_METHOD"); break;
        case DOCUMENT_URI:      return GetEnvironmentVariablePrivate("DOCUMENT_URI"); break;
        case REQUEST_URI:       return GetEnvironmentVariablePrivate("REQUEST_URI"); break;
        case QUERY_STRING:      return GetEnvironmentVariablePrivate("QUERY_STRING"); break;
        case AUTH_TYPE:         return GetEnvironmentVariablePrivate("AUTH_TYPE"); break;

        case CONTENT_TYPE:      return GetEnvironmentVariablePrivate("CONTENT_TYPE"); break;
        case CONTENT_LENGTH:    return GetEnvironmentVariablePrivate("CONTENT_LENGTH"); break;

        default: return ""; break;
    }
}

const String FastCGI::GetFastCGIVariableByName(const String &cgivar)
{
    return GetEnvironmentVariablePrivate(cgivar);
}

const String FastCGI::GetHTTPHeader(HTTP_HEADER httpheadname)
{
    // TODO
    return "";
}

const String FastCGI::GetHTTPHeaderByName(const String &httpheadname)
{
    return GetEnvironmentVariablePrivate(httpheadname);
}

const String FastCGI::GetEnvironmentVariablePrivate(const char *variable)
{
    const char *e = getenv(variable);
    return String(e ? e : "");
}

const String FastCGI::GetEnvironmentVariablePrivate(const String &variable)
{
    const char *e = getenv(variable.c_str());
    return String(e ? e : "");
}
