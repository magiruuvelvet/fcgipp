#ifndef FASTCGIRESPONSE_HPP
#define FASTCGIRESPONSE_HPP

#include "String.hpp"
#include "Vector.hpp"

class FastCGIResponse
{
public:
    using HttpHeader = std::pair<String, String>;

public:
    FastCGIResponse(const String &content_type = "text/plain; charset=utf-8");
    ~FastCGIResponse();

    void setStatus(unsigned short status);

    void addHttpHeader(const HttpHeader &header);
    void addHttpHeader(const String &name, const String &value);
    void removeHttpHeader(const HttpHeader &header);
    void removeHttpHeader(const String &name);
    bool hasHttpHeader(const HttpHeader &header) const;
    bool hasHttpHeader(const String &name) const;

    const Vector<HttpHeader> &httpHeaders() const
    { return this->headers; }

    void setBody(const String &body);
    void clearBody();

    void append(const String &str);
    void append(const char *str);
    void operator<< (const String &str) { this->body.append(str); }
    void operator<< (const char *str) { this->body.append(str); }
    void operator<< (int val) { this->body.append(std::to_string(val)); }
    void operator<< (unsigned val) { this->body.append(std::to_string(val)); }
    void operator<< (long val) { this->body.append(std::to_string(val)); }
    void operator<< (unsigned long val) { this->body.append(std::to_string(val)); }
    void operator<< (long long val) { this->body.append(std::to_string(val)); }
    void operator<< (unsigned long long val) { this->body.append(std::to_string(val)); }
    void operator<< (float val) { this->body.append(std::to_string(val)); }
    void operator<< (double val) { this->body.append(std::to_string(val)); }
    void operator<< (long double val) { this->body.append(std::to_string(val)); }

    void appendNewline();

    // debug
    const String getHeaders() const;

    // final response
    const String getResponse() const;
    void sendResponse() const;

private:

    Vector<HttpHeader> headers;
    String body;
};

#endif // FASTCGIRESPONSE_HPP
