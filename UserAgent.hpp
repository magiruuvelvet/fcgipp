#ifndef USERAGENT_HPP
#define USERAGENT_HPP

#include "String.hpp"
#include "Vector.hpp"

class UserAgent final
{
    friend class FastCGI;
    UserAgent(const String &ua_string);

public:
    String Platform;         // Mozilla
    String PlatformVersion;

    String Browser;          // Name of the web browser, last item in a well-formed UA
    String BrowserVersion;

    String GraphicalEnvironment; // X11, Wayland, Cocoa, Windows, ...
    String OperatingSystem;      // OS name
    String OSArch;               // OS architecture


    String DEBUG;

    using Engine = std::pair<String, String>; // name/version
    Vector<Engine> Engines;        // all items, except the last one in a well-formed UA

public: // helper_functions

    // Environment checks
    bool isDesktop() const;
    bool isMobile() const;
    bool isWearable() const;
    bool isGamingConsole() const;

    // check if the user agent submitted some kind of Web Engine
    // can it render HTML+CSS ?
    bool hasWebEngine() const;

    // common applications
    bool isDownloadManager() const;
    bool isCurl() const;
    bool isWget() const;

    // ...

private:
    bool hasEngine(const String &name) const;
    bool isBrowser(const String &name) const;
};

#endif // USERAGENT_HPP
