# fcgi++ application library

A work-in-progress experiment to implement a tiny application library to quickly develop FastCGI web applications in C++.

### Development

This library originally started as a "just for fun" attempt to see how far I can get with unusual web application development. This turned out better than I fought and the code I wrote is quite usable already and I don't want to trash it. I will try to work on it in my free time and make a stable library which can be used in production out of it.

The idea behind this library is that you don't need to manually verify the process environment and parse all the CGI and HTTP stuff. Like the REQUEST TYPE for example, or if the application is served via a TLS connection, etc...

### Testing

I make use of the [Caddy](https://caddyserver.com/) web server which has easy and human-readable config files and fully automated HTTPS via Let's Encrypt. Getting a FastCGI connection is easy as writting just a single line of code. (`fastcgi {source} {target_server}`)
Say goodbye to cryptic and unreadable configuration files ;)

**Caddyfile**
```plain
# /etc/hosts
# 127.0.0.1 website.local
#
#
# spawn a FastCGI server using
# $ fcgiwrap -s tcp:127.0.0.1:10222
#
# the environment of the fcgiwrap server doesn't matter at all
# Caddy sets all the variables correctly automagically :)
# the application must be placed in the document root though
#

http://website.local:10889 {

    # Auth tests
    #basicauth / test test

    # FastCGI server to invoke the application
    fastcgi / 127.0.0.1:10222 {
        index fcgipp
    }

    # Catch all in HTTP path and store it into a query string; ?path={path}
    rewrite {
        r ^/(.*)
        to /?path={1}
    }
}

# TLS testing
# note that the Caddy http->s redirect doesn't work for ports and sends garbage to the client
https://website.local:10890 {

    # generate a temporary in-memory certificate
    # must skip SSL verification in clients during development
    # curl -k ...
    tls self_signed

    # Auth tests
    #basicauth / test test

    fastcgi / 127.0.0.1:10222 {
        index fcgipp
    }
}
```

Note to the Caddyfile: Caddy defaults the document `root` to the current working directory when a docroot is required. So it doesn't explicitly needs to be specified.
