#ifndef STRING_HPP
#define STRING_HPP

#include <string>
#include "Vector.hpp"

class String : public std::string
{
public:
    using std::string::string;
    String() = default;
    String(const char *cs);
    String(const std::string &ss);
    String(const String&) = default;
    String &operator=(const String&) = default;
    String &operator=(const char *cs);
    String &operator=(const std::string &ss);
    const char *operator*() const;

    void to_lower();
    const String lower() const;
    void to_upper();
    const String upper() const;

    bool case_insensitive_compare(const String &other) const;

    void trim();
    void trim_front();
    void trim_back();
    const String simplified() const;

    bool starts_with(const String &s) const;
    bool starts_with(const char *cs) const;
    bool not_starts_with(const String &s) const;
    bool not_starts_with(const char *cs) const;

    bool ends_with(const String &s) const;
    bool ends_with(const char *cs) const;
    bool not_ends_with(const String &s) const;
    bool not_ends_with(const char *cs) const;

    std::size_t count(const char c) const;
    std::size_t count(const String &substr) const;
    std::size_t count(const char *substr) const;

    Vector<String> split(const char sep) const;

    bool contains(const char c) const;
    bool contains(const String &substr) const;
    bool contains(const char *substr) const;

private:
    static bool u_isspace(char ch);
};

#endif // STRING_HPP
