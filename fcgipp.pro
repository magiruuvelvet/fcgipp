TEMPLATE = app
CONFIG += console strict_c++ c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    FastCGI.cpp \
    FastCGIResponse.cpp \
    UserAgent.cpp \
    String.cpp \
    Vector.cpp

HEADERS += \
    FastCGI.hpp \
    FastCGIResponse.hpp \
    UserAgent.hpp \
    String.hpp \
    Vector.hpp
