#include "String.hpp"

#include <algorithm>
#include <sstream>

bool String::u_isspace(char ch)
{
    return std::isspace(static_cast<unsigned char>(ch));
}

String::String(const char *cs)
    : std::string(cs)
{
}

String::String(const std::string &ss)
{
    dynamic_cast<std::string&>(*this) = ss;
}

String &String::operator=(const char *cs)
{
    dynamic_cast<std::string&>(*this) = cs;
    return *this;
}

String &String::operator=(const std::string &ss)
{
    dynamic_cast<std::string&>(*this) = ss;
    return *this;
}

const char *String::operator*() const
{
    return this->c_str();
}

void String::to_lower()
{
    std::transform(begin(), end(), begin(), [](unsigned char c) {
        return std::tolower(c);
    });
}

const String String::lower() const
{
    String s(*this);
    s.to_lower();
    return s;
}

void String::to_upper()
{
    std::transform(begin(), end(), begin(), [](unsigned char c) {
        return std::toupper(c);
    });
}

const String String::upper() const
{
    String s(*this);
    s.to_upper();
    return s;
}

bool String::case_insensitive_compare(const String &other) const
{
    return this->lower() == other.lower();
}

void String::trim()
{
    this->trim_front();
    this->trim_back();
}

void String::trim_front()
{
    while (length() > 0)
    {
        if (!u_isspace(this->front()))
        {
            break;
        }
        this->erase(begin());
    }
}

void String::trim_back()
{
    while (length() > 0)
    {
        if (!u_isspace(this->back()))
        {
            break;
        }
        this->erase(--end());
    }
}

const String String::simplified() const
{
    String s(*this);
    s.trim();
    return s;
}

bool String::starts_with(const String &s) const
{
    // if the given string is larger than the current string return false
    if (s.size() > size())
        return false;

    // compare from begin to s.size()
    return compare(0, s.size(), s) == 0;
}

bool String::starts_with(const char *cs) const
{
    return this->starts_with(String(cs));
}

bool String::not_starts_with(const String &s) const
{
    return !this->starts_with(s);
}
bool String::not_starts_with(const char *cs) const
{
    return !this->starts_with(cs);
}

bool String::ends_with(const String &s) const
{
    // if the given string is larger than the current string return false
    if (s.size() > size())
        return false;

    // compare from the position which matches the s.size() to the end
    return compare(size() - s.size(), s.size(), s) == 0;
}

bool String::ends_with(const char *cs) const
{
    return this->ends_with(String(cs));
}

bool String::not_ends_with(const String &s) const
{
    return !this->ends_with(s);
}
bool String::not_ends_with(const char *cs) const
{
    return !this->ends_with(cs);
}

std::size_t String::count(const char c) const
{
    std::size_t count = 0;
    for (auto&& ch : *this)
        ch == c ? count++ : count;
    return count;
}

std::size_t String::count(const String &substr) const
{
    std::size_t count = 0;
    for (std::size_t i = 0; i < this->size(); i++)
        if (this->substr(i, substr.size()) == substr)
            count++;
    return count;
}
std::size_t String::count(const char *substr) const
{
    return this->count(String(substr));
}

Vector<String> String::split(const char sep) const
{
    std::stringstream ss(*this);
    String item;
    Vector<String> tokens;
    while (std::getline(ss, item, sep))
        tokens.push_back(item);
    return tokens;
}

bool String::contains(const char c) const
{
    return this->find(c) != String::npos;
}

bool String::contains(const String &substr) const
{
    return this->find(substr) != String::npos;
}
bool String::contains(const char *substr) const
{
    return this->find(substr) != String::npos;
}
