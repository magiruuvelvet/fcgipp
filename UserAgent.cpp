#include "UserAgent.hpp"

// This parser is far from being finished and still needs a lot of work.

UserAgent::UserAgent(const String &ua_string)
{
    String ua = ua_string.simplified();

    // don't do anything if the UA string is empty
    if (ua.empty())
        return;

    if (ua.starts_with("("))
        return;

    try { // avoid string out-of-bound exceptions, keep everything empty or cancel UA parsing on errors

    if (ua.count('/') == 0)
    {
        this->Browser = ua;
    }
    else if (ua.count('/') == 1)
    {
        auto browser = ua.split('/');
        this->Browser = browser.at(0);
        this->BrowserVersion = browser.at(1);
    }
    else
    {
        auto pltfrm_split = ua.find_first_of(' ');
        String pltfrm = ua.substr(0, pltfrm_split);
        auto platform = pltfrm.split('/');
        this->Platform = platform.at(0);
        this->PlatformVersion = platform.at(1);

        if (ua.contains('(') && ua.contains(')'))
        {
            // substr: start, end - start
            String ft = ua.substr(ua.find_first_of('(') + 1, ua.find_first_of(')') - ua.find_first_of('(') - 1);
            auto features = ft.split(';');
            for (auto &i : features) i.trim();

            // Graphical Environment
            if (features.contains("X11"))
                this->GraphicalEnvironment = "X11";
            else if (features.contains("Wayland"))
                this->GraphicalEnvironment = "Wayland";

            int os_arch_pos;
            if (strvector_str_contains(features, " ", &os_arch_pos))
            {
                // get OS and arch string
                auto os_arch = features.at(os_arch_pos);

                // OS + arch in one string
                if (os_arch.starts_with("Linux") ||
                    os_arch.contains("BSD"))
                {
                    auto split = os_arch.split(' ');
                    this->OperatingSystem = split.at(0);
                    this->OSArch = split.at(1);
                }

                // OS and arch in separate string
                else if (os_arch.starts_with("Windows"))
                {
                    this->OperatingSystem = os_arch;
                    this->GraphicalEnvironment = "Windows";
                    features.contains("WOW64") ? this->OSArch = "x86_64" : this->OSArch = "x86";
                }
                else if (os_arch.contains("Intel Mac OS X"))
                {
                    this->OperatingSystem = os_arch;
                    // TODO: x11, cocoa <-- macosx version
                    this->GraphicalEnvironment = "Cocoa";
                    this->OSArch = "x86_64";
                }
            }
        }

        auto brwser_split = ua.find_last_of(' ');
        String brwser = ua.substr(brwser_split + 1);
        auto browser = brwser.split('/');
        this->Browser = browser.at(0);
        this->BrowserVersion = browser.at(1);

        auto eng_start = ([&]{
            if (ua.contains('(') && ua.contains(')'))
                return ua.find_first_of(')') + 1;
            else
                return ua.find_first_of(' ');
        })();
        auto eng_end = brwser_split;

        //auto engines = String(ua.substr(eng_start + 1, eng_end - eng_start - 1)).split(' ');
        String engines = String(ua.substr(eng_start + 1, eng_end - eng_start - 1));
        while (engines.contains('(') && engines.contains(')'))
        {
            String left = engines.substr(0, engines.find_first_of('('));
            String right = engines.substr(engines.find_first_of(')') + 2);
            engines = left + right;
        }
        for (auto&& e : engines.split(' '))
        {
            auto tmp = e.split('/');
            this->Engines.push_back(Engine(tmp[0], tmp[1]));
        }
    }

    } catch (...) {
        // silently ignore parsing errors
        // user agent strings are way to complex sometimes
    }
}

bool UserAgent::isDesktop() const
{
    // TODO
    return bool();
}

bool UserAgent::isMobile() const
{
    // TODO
    return bool();
}

bool UserAgent::isWearable() const
{
    // TODO
    return bool();
}

bool UserAgent::isGamingConsole() const
{
    // TODO
    return bool();
}

bool UserAgent::hasWebEngine() const
{
    // TODO: can still be extended

    const bool engine =
        !this->Platform.empty() ||
        this->hasEngine("AppleWebKit") ||
        this->hasEngine("Gecko") ||
        this->hasEngine("Chrome") ||
        this->hasEngine("QtWebEngine");

    const bool browser =
        this->isBrowser("Firefox") ||
        this->isBrowser("Safari") ||
        this->isBrowser("Chrome") ||
        this->isBrowser("Vivaldi");

    return engine || browser;
}

bool UserAgent::isDownloadManager() const
{
    // TODO
    return bool();
}

bool UserAgent::isCurl() const
{
    return this->Browser.case_insensitive_compare("curl");
}

bool UserAgent::isWget() const
{
    return this->Browser.case_insensitive_compare("wget");
}

bool UserAgent::hasEngine(const String &name) const
{
    for (auto&& e : this->Engines)
        if (e.first.case_insensitive_compare(name))
            return true;
    return false;
}

bool UserAgent::isBrowser(const String &name) const
{
    return (this->Browser.case_insensitive_compare(name));
}
