#ifndef FASTCGI_HPP
#define FASTCGI_HPP

#include "String.hpp"

class UserAgent;
class FastCGIResponse;

class FastCGI final
{
    FastCGI() {}
public:

    // Checks if the application was invoked by a FastCGI server or standalone
    static bool IsRunningUnderFastCGI();

    // Checks if the application is served via TLS (HTTPS)
    static bool IsServedViaTLS();

    // Request by the client to use a encrypted TLS (HTTPS) connection
    static bool UpgradeInsecureRequests();

    // Has client DNT enabled?
    static bool DoNotTrackEnabled();

    // Get user agent string; be aware that this can be empty
    static const String &UserAgentString();
    static const UserAgent &UserAgent();

    // For debugging purposes, pretty-prints the entire CGI-specific environment without HTTP headers
    static void PrintCGIServerInfo(FastCGIResponse &res);

    // For debugging purposes, pretty-print all HTTP Headers (HTTP_*)
    // Be aware this may contain plain text passwords when using the HTTP basicauth feature !!!
    enum DEBUG_HTTP_HEADERS {
        All, Standard, Custom
    };
    static void PrintHTTPHeaders(FastCGIResponse &res, DEBUG_HTTP_HEADERS = All);

    // For debugging purposes, print entire process environment (output may be ugly)
    // Be aware this may contain personal information, don't use this in production !!!
    static void PrintProcessEnvironment(FastCGIResponse &res);

public:
    enum HTTP_REQUEST_METHOD {
        HEAD, OPTIONS, GET, POST, PUT, PATCH, DELETE, TRACE, CONNECT,
        INVALID // invalid request method
    };

    enum FASTCGI_VARIABLE {
        // FastCGI Server Info
        GATEWAY_INTERFACE,                              // Gateway Interface Type and Version
        REMOTE_ADDR,                                    // Address of the remote CGI server
        REMOTE_IDENT,
        REMOTE_USER,                                    // HTTP basicauth ── Username; otherwise empty
        PATH_INFO,
        PATH_TRANSLATED,

        // FastCGI Host/Upstream Server Info
        SERVER_PROTOCOL,                                // Server Protocol and Version, examples: HTTP/1.1 HTTP/2
        SERVER_SOFTWARE,                                // Server Software, example: Caddy/0.10.10
        SERVER_NAME,                                    // Domain Name, example: sub.example.com
        DOMAIN_NAME,                                    // (alias for SERVER_NAME)
        SERVER_PORT,                                    // Port number were the Server is running
        SCRIPT_NAME,                                    // HTTP Path to the CGI script/application
        DOCUMENT_ROOT,                                  // Path were the server is looking for files

        // FastCGI HTTP Properties
        REQUEST_METHOD,                                 // HTTP Request Method, HEAD, GET, PUT, etc...
        DOCUMENT_URI,                                   // HTTP Path, usually the SCRIPT_NAME if no special CGI handling is done; contains the invisible rewritten URI on server
        REQUEST_URI,                                    // Client requested HTTP Path
        QUERY_STRING,                                   // Everything after the first '?' (excluding the '?')
        AUTH_TYPE,                                      // [always empty?]

        // FastCGI HTTP Content Info
        CONTENT_TYPE,
        CONTENT_LENGTH,
    };

    // TODO
    enum HTTP_HEADER {
    };

    // Get application path on local filesystem
    static const String ApplicationPath();

    // Most common HTTP request methods
    static HTTP_REQUEST_METHOD GetHttpRequestMethod();

    // HTTP allows custom request methods
    static const String GetHttpRequestMethodRaw();

    // Has query string key
    static bool QueryStringHasKey(const String &key);

    // Check if the given query string key has a value (=)
    static bool QueryStringKeyHasValue(const String &key);

    // Get query string value
    static const String QueryStringValue(const String &key);

    // FastCGI environment
    static const String GetFastCGIVariable(FASTCGI_VARIABLE cgivar);
    static const String GetFastCGIVariableByName(const String &cgivar);

    // HTTP headers (environment variables set by the fcgi server)
    static const String GetHTTPHeader(HTTP_HEADER httpheadname);
    static const String GetHTTPHeaderByName(const String &httpheadname);

private:

    static const String GetEnvironmentVariablePrivate(const char *variable);
    static const String GetEnvironmentVariablePrivate(const String &variable);
};

#endif // FASTCGI_HPP
