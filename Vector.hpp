#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <vector>

class String;

template<typename T>
class Vector : public std::vector<T>
{
public:
    using std::vector<T>::vector;
    Vector() = default;

    Vector &operator=(const Vector&) = default;
    Vector &operator=(const std::vector<T> &vec)
    { *this = vec; }
    const T *operator*() const
    { return this->data(); }

    bool contains(const String &str) const
    {
        for (auto&& elem : *this)
            if (elem == str)
                return true;
        return false;
    }
};

bool strvector_str_contains(const Vector<String> &strvec, const String &str, int *pos = nullptr);
bool strvector_str_starts_with(const Vector<String> &strvec, const String &str, int *pos = nullptr);

#endif // VECTOR_HPP
